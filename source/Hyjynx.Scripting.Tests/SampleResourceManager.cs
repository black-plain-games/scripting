﻿using System.Collections.Generic;

namespace Hyjynx.Scripting.Tests
{
    public class SampleResourceManager : IResourceManager
    {
        public void Add(string identifier, object value)
        {
            _resources.Add(identifier, value);
        }

        public object Retrieve(string identifier)
        {
            return _resources[identifier];
        }

        public bool Contains(string identifier)
        {
            return _resources.ContainsKey(identifier);
        }

        public void Remove(string identifier)
        {
            _resources.Remove(identifier);
        }

        private readonly IDictionary<string, object> _resources = new Dictionary<string, object>();
    }
}