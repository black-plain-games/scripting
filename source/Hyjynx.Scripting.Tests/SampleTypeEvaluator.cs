﻿using System;
using System.Linq;
using System.Reflection;

namespace Hyjynx.Scripting.Tests
{
    public class SampleTypeEvaluator : ITypeEvaluator
    {
        public Type GetType(string name)
        {
            return Assembly.GetExecutingAssembly().GetTypes().FirstOrDefault(t => string.Compare(t.Name, name, true) == 0);
        }
    }
}