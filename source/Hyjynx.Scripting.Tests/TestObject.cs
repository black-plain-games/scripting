﻿namespace Hyjynx.Scripting.Tests
{
    public class TestObject
    {
        public int A { get; set; }

        public string B { get; set; }

        public decimal C { get; set; }

        public int D { get; set; }

        public TestObject E { get; set; }

        public TestObject()
        {
            A = 1;

            B = "2";

            C = 3;
        }

        public TestObject(string b)
        {
            A = 1;

            B = b;

            C = 3;
        }

        public TestObject(int a, string b, decimal c)
        {
            A = a;

            B = b;

            C = c;
        }

        public TestObject(TestObject e)
        {
            E = e;
        }

        public TestObject(int a, TestObject e, string b)
        {
            A = a;

            E = e;

            B = b;
        }

        public void SetD(int d)
        {
            D = d;
        }

        public int GetA()
        {
            return A;
        }
    }
}