﻿using System.Collections.Generic;
using Hyjynx.Scripting.Generators;
using Hyjynx.Scripting.Identifiers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Hyjynx.Scripting.Tests
{
    [TestClass]
    public class MakeTests
    {
        private IScriptParser _parser;

        private IResourceManager _resources;

        private ITypeEvaluator _typeEvaluator;

        private CommandGeneratorContainer _commandGenerator;

        private TokenIdentifierContainer _tokenIdentifier;

        [TestInitialize]
        public void Initialize()
        {
            _resources = new SampleResourceManager();

            _typeEvaluator = new SampleTypeEvaluator();

            _commandGenerator = new CommandGeneratorContainer(_resources, _typeEvaluator);

            _tokenIdentifier = new TokenIdentifierContainer();

            _parser = new ScriptParser(_commandGenerator, _tokenIdentifier);
        }

        [TestMethod]
        public void Parser_Parse_IsNotNull()
        {
            var objectName = "Sample Object";

            var scriptContent = $"MAKE TestObject NAMED {Safe(objectName)} GO";
            
            var script = _parser.Parse(scriptContent);

            Assert.IsNotNull(script);
        }

        [TestMethod]
        public void Script_Execute_MakesResource()
        {
            var objectName = "Sample Object";

            var scriptContent = $"MAKE TestObject NAMED {Safe(objectName)} GO";

            var script = _parser.Parse(scriptContent);
            
            Assert.IsFalse(_resources.Contains(objectName), "Resources should not contain object before script runs.");

            script.Execute();

            Assert.IsTrue(_resources.Contains(objectName), "Resources should contain object.");
        }

        [TestMethod]
        public void Script_Execute_MakesResource_WithParameters()
        {
            var objectName = "Sample Object \"Coolio\"";

            var scriptContent = $"MAKE TestObject NAMED {Safe(objectName)} WITH b AS NULL GO";

            var script = _parser.Parse(scriptContent);

            script.Execute();

            var testObject = _resources.Retrieve(objectName) as TestObject;

            Assert.IsNull(testObject.B, "Property B should be null.");
        }

        [TestMethod]
        public void Script_Execute_MakesResource_WithParameters_2()
        {
            var objectName = "Sample Object";

            var aValue = 123;

            var bValue = "Yo Dawg!";

            var cValue = 456.789m;

            var scriptContent = $"MAKE TestObject NAMED {Safe(objectName)} WITH a AS {aValue} b AS \"{bValue}\" c AS {cValue} GO";

            var script = _parser.Parse(scriptContent);

            script.Execute();

            var testObject = _resources.Retrieve(objectName) as TestObject;

            Assert.IsNotNull(testObject, "Object should have been created.");

            Assert.AreEqual(aValue, testObject.A, "Property A should have been set properly.");

            Assert.AreEqual(bValue, testObject.B, "Property B should have been set properly.");

            Assert.AreEqual(cValue, testObject.C, "Property C should have been set properly.");
        }

        [TestMethod]
        public void Script_Execute_MakesResource_Nested()
        {
            var objectName = "Sample Object";

            var aValue = 111;

            var bValue = "Hello World!";

            var cValue = 98789.123125m;

            var scriptContent = $"MAKE TestObject NAMED {Safe(objectName)} WITH e AS MAKE TestObject WITH a AS {aValue} b AS {Safe(bValue)} c AS {cValue} GO GO";

            var script = _parser.Parse(scriptContent);

            script.Execute();

            var testObject = _resources.Retrieve(objectName) as TestObject;

            Assert.IsNotNull(testObject, "Object should have been created.");

            Assert.IsNotNull(testObject.E, "Nested object should have been created.");

            Assert.AreEqual(aValue, testObject.E.A, "Property A should have been set properly.");

            Assert.AreEqual(bValue, testObject.E.B, "Property B should have been set properly.");

            Assert.AreEqual(cValue, testObject.E.C, "Property C should have been set properly.");
        }

        [TestMethod]
        public void Script_Execute_MakesResource_Nested_2()
        {
            var objectName = "Sample Object";

            var a1Value = 467342;

            var b1Value = "Outer Poop!";

            var a2Value = 643752;

            var b2Value = "Poop on the inside!";

            var c2Value = 45245.78345m;

            var scriptContent = string.Format(
@"MAKE TestObject NAMED {0} WITH
    a AS {1}
    e AS
        MAKE TestObject WITH
            a AS {2}
            b AS {3}
            c AS {4}
        GO
    b AS {5}
GO",
Safe(objectName), a1Value, a2Value, Safe(b2Value), c2Value, Safe(b1Value));

            var script = _parser.Parse(scriptContent);

            script.Execute();

            var testObject = _resources.Retrieve(objectName) as TestObject;

            Assert.IsNotNull(testObject, "Object should have been created.");

            Assert.AreEqual(a1Value, testObject.A, "Property A on outer object should have been set properly.");

            Assert.AreEqual(b1Value, testObject.B, "Property B on outer object should have been set properly.");

            Assert.IsNotNull(testObject.E, "Nested object should have been created.");

            Assert.AreEqual(a2Value, testObject.E.A, "Property A on inner object should have been set properly.");

            Assert.AreEqual(b2Value, testObject.E.B, "Property B on inner object should have been set properly.");

            Assert.AreEqual(c2Value, testObject.E.C, "Property C on inner object should have been set properly.");
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void Script_Execute_Require_Fails()
        {
            var objectName = "Sample Object";

            var scriptContent = $"REQUIRES [{objectName}] GO";

            var script = _parser.Parse(scriptContent);

            script.Execute();
        }

        [TestMethod]
        public void Script_Execute_Require_Succeeds()
        {
            var objectName = "Sample Object";

            var scriptContent = $"REQUIRES [{objectName}] GO";

            _resources.Add(objectName, new object());

            var script = _parser.Parse(scriptContent);

            script.Execute();
        }

        [TestMethod]
        public void Script_Execute_Unname_Succeeds()
        {
            var objectName = "Sample Object";

            var scriptContent = $"UNNAME {Safe(objectName)} GO";

            _resources.Add(objectName, new object());

            var script = _parser.Parse(scriptContent);

            script.Execute();

            Assert.IsFalse(_resources.Contains(objectName), "Unnaming a resource should remove it.");
        }

        [TestMethod]
        public void Script_Execute_Set_Succeeds()
        {
            var objectName = "Sample Object";

            var dValue = 5146214;

            var scriptContent = $"SET D ON [{objectName}] AS {dValue} GO";

            var data = new TestObject();

            _resources.Add(objectName, data);

            var script = _parser.Parse(scriptContent);

            Assert.AreNotEqual(dValue, data.D, "D should not be set before the script runs");

            script.Execute();

            Assert.AreEqual(dValue, data.D, "D should be set after the script runs");
        }

        [TestMethod]
        public void Script_Execute_Call_Succeeds()
        {
            var objectName = "Sample Object";

            var dValue = 5146214;

            var scriptContent = $"CALL SetD ON [{objectName}] WITH d AS {dValue} GO";

            var data = new TestObject();

            _resources.Add(objectName, data);

            var script = _parser.Parse(scriptContent);

            Assert.AreNotEqual(dValue, data.D, "D should not be set before the script runs");

            script.Execute();

            Assert.AreEqual(dValue, data.D, "D should be set after the script runs");
        }

        [TestMethod]
        public void Script_Execute_Call_Nested_Succeeds()
        {
            var objectName = "Sample Object";

            var dValue = 5146214;

            var scriptContent = $"CALL SetD ON [{objectName}] WITH d AS CALL GetA ON [{objectName}] GO GO";

            var data = new TestObject();

            data.A = dValue;

            _resources.Add(objectName, data);

            var script = _parser.Parse(scriptContent);

            Assert.AreNotEqual(dValue, data.D, "D should not be set before the script runs");

            script.Execute();

            Assert.AreEqual(dValue, data.D, "D should be set after the script runs");
        }

        [TestMethod]
        public void Script_Execute_Read_Succeeds()
        {
            var objectName = "Sample Object";

            var aValue = 561351;

            var scriptContent = $"SET D ON [{objectName}] AS READ A ON [{objectName}] GO GO";

            var data = new TestObject();

            data.A = aValue;

            _resources.Add(objectName, data);

            var script = _parser.Parse(scriptContent);

            script.Execute();

            Assert.AreEqual(aValue, data.D, "D should be set after the script runs");
        }

        private string Safe(string name)
        {
            return string.Format("\"{0}\"", name.Replace("\"", "\\\""));
        }
    }
}