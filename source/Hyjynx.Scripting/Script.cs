﻿using System;
using System.Collections.Generic;

namespace Hyjynx.Scripting
{
    public class Script
    {
        public string ErrorMessage { get; set; }

        public ICollection<IScriptCommand> Commands { get; }

        public Script()
        {
            Commands = new List<IScriptCommand>();
        }

        public void Execute()
        {
            if(!string.IsNullOrWhiteSpace(ErrorMessage))
            {
                throw new InvalidOperationException("Cannot execute a script containing errors.");
            }

            foreach(var command in Commands)
            {
                command.GetValue();
            }
        }
    }
}