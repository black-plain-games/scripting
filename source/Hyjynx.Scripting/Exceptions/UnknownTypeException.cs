﻿using System;

namespace Hyjynx.Scripting.Exceptions
{
    public class UnknownTypeException : Exception
    {
        public UnknownTypeException(string typeName, int index)
            : base($"Unknown type '{typeName}' at token index {index}.")
        {
        }
    }
}