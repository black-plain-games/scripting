﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Exceptions
{
    public class InvalidParameterListException : Exception
    {
        public InvalidParameterListException(Type type, IEnumerable<ScriptCommandParameter> parameters)
            : base($"Type '{type.Name}' does not contain an operation with parameter list ({string.Join(",", parameters.Select(p => p.ParameterName))}).")
        {
        }
    }
}