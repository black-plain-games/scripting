﻿using System;

namespace Hyjynx.Scripting.Exceptions
{
    public class UnexpectedTokenException : Exception
    {
        public UnexpectedTokenException(ScriptToken token, int index)
            : base($"Unexpected token '{token}' at token index {index}.")
        {
        }
    }
}