﻿using System;

namespace Hyjynx.Scripting.Exceptions
{
    public class InvalidSyntaxException : Exception
    {
        public InvalidSyntaxException(string message, int tokenIndex)
            : base(string.Format("{0} At index {1}.", message, tokenIndex))
        {
        }
    }
}