﻿using System;

namespace Hyjynx.Scripting
{
    internal static class Utilities
    {
        public static bool CanConvertTo(Type targetType, object value, IFormatProvider culture)
        {
            if (targetType.IsAssignableFrom(value.GetType()))
            {
                return true;
            }

            if(targetType.IsEnum && (value is string || IsWholeNumberType(value)))
            {
                return true;
            }
            
            try
            {
                var typedValue = Convert.ChangeType(value, targetType, culture);

                return typedValue != null;
            }
            catch
            {
                return false;
            }
        }

        public static bool IsWholeNumberType(object value)
        {
            return value != null && IsWholeNumberType(value.GetType());
        }

        public static bool IsWholeNumberType(Type type)
        {
            return type == typeof(byte) ||
                type == typeof(sbyte) ||
                type == typeof(short) ||
                type == typeof(ushort) ||
                type == typeof(int) ||
                type == typeof(uint) ||
                type == typeof(long) ||
                type == typeof(ulong);
        }
    }
}