﻿namespace Hyjynx.Scripting
{
    public class ScriptToken
    {
        public ScriptTokenType TokenType { get; set; }

        public string Value { get; set; }

        public override string ToString()
        {
            return $"{TokenType} : {Value}";
        }
    }
}