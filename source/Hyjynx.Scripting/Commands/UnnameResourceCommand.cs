﻿namespace Hyjynx.Scripting.Commands
{
    public class UnnameResourceCommand : IScriptCommand
    {
        public ScriptCommandType CommandType => ScriptCommandType.UnnameResource;

        public UnnameResourceCommand(IResourceManager resources, string resourceName)
        {
            _resources = resources;

            _resourceName = resourceName;
        }

        public object GetValue()
        {
            _resources.Remove(_resourceName);

            return null;
        }

        private readonly IResourceManager _resources;

        private readonly string _resourceName;
    }
}