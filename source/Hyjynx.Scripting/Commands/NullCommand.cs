﻿namespace Hyjynx.Scripting
{
    public class NullCommand : IScriptCommand
    {
        private NullCommand()
        {
        }

        public static NullCommand Instance => _instance;

        public ScriptCommandType CommandType => ScriptCommandType.Null;

        public object GetValue()
        {
            return null;
        }

        private static readonly NullCommand _instance = new NullCommand();
    }
}