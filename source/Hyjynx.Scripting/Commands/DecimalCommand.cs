﻿namespace Hyjynx.Scripting.Commands
{
    public class DecimalCommand : IScriptCommand
    {
        public ScriptCommandType CommandType => ScriptCommandType.Decimal;

        public DecimalCommand(string value)
        {
            _value = decimal.Parse(value);
        }

        public object GetValue()
        {
            return _value;
        }

        private readonly decimal _value;
    }
}