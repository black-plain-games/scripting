﻿using System.Collections.Generic;

namespace Hyjynx.Scripting.Commands
{
    public class RequiredResourceCommand : IScriptCommand
    {
        public ScriptCommandType CommandType => ScriptCommandType.RequiredResource;

        public RequiredResourceCommand(IResourceManager resources, string resourceName)
        {
            _resources = resources;

            _resourceName = resourceName;
        }

        public object GetValue()
        {
            if(!_resources.Contains(_resourceName))
            {
                throw new KeyNotFoundException($"Resource '{_resourceName}' is required and was not found.");
            }

            return null;
        }

        private readonly IResourceManager _resources;

        private readonly string _resourceName;
    }
}