﻿namespace Hyjynx.Scripting
{
    public interface IScriptCommand : IScriptValue
    {
        ScriptCommandType CommandType { get; }
    }
}