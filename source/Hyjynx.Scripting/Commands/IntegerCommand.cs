﻿using System;

namespace Hyjynx.Scripting.Commands
{
    public class IntegerCommand : IScriptCommand
    {
        public ScriptCommandType CommandType => ScriptCommandType.Integer;

        public IntegerCommand(string value)
        {
            _value = int.Parse(value);
        }

        public object GetValue()
        {
            return _value;
        }

        private readonly int _value;
    }
}