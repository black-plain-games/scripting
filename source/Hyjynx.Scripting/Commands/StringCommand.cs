﻿namespace Hyjynx.Scripting.Commands
{
    public class StringCommand : IScriptCommand
    {
        public ScriptCommandType CommandType => ScriptCommandType.String;

        public StringCommand(string value)
        {
            _value = value;
        }

        public object GetValue()
        {
            return _value;
        }

        private readonly string _value;
    }
}