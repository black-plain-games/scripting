﻿using System;
using System.Linq;

namespace Hyjynx.Scripting.Commands
{
    public class ReadValueCommand : IScriptCommand
    {
        public ScriptCommandType CommandType => ScriptCommandType.ReadValue;

        public ReadValueCommand(IResourceManager resources, string resourceName, string propertyName)
        {
            _resources = resources;

            _resourceName = resourceName;

            _propertyName = propertyName;
        }

        public object GetValue()
        {
            var resource = _resources.Retrieve(_resourceName);

            var targetType = resource.GetType();

            var properties = targetType.GetProperties();

            var property = properties.FirstOrDefault(p => string.Compare(p.Name, _propertyName, StringComparison.CurrentCultureIgnoreCase) == 0);
            
            if (property != null)
            {
                return property.GetValue(resource, null);
            }

            var fields = targetType.GetFields();

            var field = fields.FirstOrDefault(f => string.Compare(f.Name, _propertyName, StringComparison.CurrentCultureIgnoreCase) == 0);

            if (field == null)
            {
                throw new ArgumentException($"No property or field named '{_propertyName}' exists on '{_resourceName}' of type '{targetType}'.");
            }

            return field.GetValue(resource);
        }

        private readonly IResourceManager _resources;

        private readonly string _resourceName;

        private readonly string _propertyName;
    }
}