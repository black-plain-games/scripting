﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Hyjynx.Scripting.Exceptions;

namespace Hyjynx.Scripting.Commands
{
    public class MakeResourceCommand : IScriptCommand
    {
        public ScriptCommandType CommandType => ScriptCommandType.MakeResource;

        public MakeResourceCommand(IResourceManager resources, Type targetType, string resourceName, IEnumerable<ScriptCommandParameter> parameters)
        {
            _resources = resources;

            _targetType = targetType;

            _resourceName = resourceName;

            _parameters = parameters;
        }
        
        public object GetValue()
        {
            var parameterValues = GetParameterValues();

            var constructorInfo = GetConstructor(parameterValues);

            var parameters = GetParameterValues(constructorInfo, parameterValues);

            var value = constructorInfo.Invoke(parameters);

            if(_resourceName != null)
            {
                _resources.Add(_resourceName, value);
            }

            return value;
        }

        private ConstructorInfo GetConstructor(IDictionary<string, object> parameterValues)
        {
            var constructors = _targetType.GetConstructors();
            
            foreach(var constructor in constructors)
            {
                var parameters = new List<ParameterInfo>(constructor.GetParameters());

                var isMatch = true;

                foreach (var parameter in parameterValues)
                {
                    var cParameter = parameters.FirstOrDefault(p => string.Compare(p.Name, parameter.Key, StringComparison.CurrentCultureIgnoreCase) == 0);

                    if (cParameter == null)
                    {
                        isMatch = false;

                        break;
                    }

                    if(parameter.Value != null &&
                       !Utilities.CanConvertTo(cParameter.ParameterType, parameter.Value, _culture))
                    {
                        isMatch = false;

                        break;
                    }

                    parameters.Remove(cParameter);
                }

                if(isMatch && parameters.Any())
                {
                    foreach(var parameter in parameters)
                    {
                        if(!parameter.IsOptional)
                        {
                            isMatch = false;

                            break;
                        }
                    }
                }

                if(isMatch)
                {
                    return constructor;
                }
            }

            throw new InvalidParameterListException(_targetType, _parameters);
        }

        private IDictionary<string, object> GetParameterValues()
        {
            var output = new Dictionary<string, object>();

            foreach(var parameter in _parameters)
            {
                output.Add(parameter.ParameterName, parameter.Value.GetValue());
            }

            return output;
        }

        private object[] GetParameterValues(ConstructorInfo constructor, IDictionary<string, object> parameters)
        {
            var output = new List<object>();

            var constructorParameters = constructor.GetParameters();

            foreach (var constructorParameter in constructorParameters)
            {
                var parameter = parameters.FirstOrDefault(p => string.Compare(constructorParameter.Name, p.Key, StringComparison.CurrentCultureIgnoreCase) == 0);

                object parameterValue = null;

                if (parameter.Key != null)
                {
                    if(parameter.Value == null)
                    {
                        parameterValue = null;
                    }
                    else if(constructorParameter.ParameterType.IsAssignableFrom(parameter.Value.GetType()))
                    {
                        parameterValue = parameter.Value;                        
                    }
                    else if(constructorParameter.ParameterType.IsEnum)
                    {
                        parameterValue = Enum.Parse(constructorParameter.ParameterType, parameter.Value.ToString(), true);
                    }
                    else
                    {
                        parameterValue = Convert.ChangeType(parameter.Value, constructorParameter.ParameterType, _culture);
                    }
                }
                else
                {
                    parameterValue = constructorParameter.DefaultValue;
                }

                output.Add(parameterValue);
            }

            return output.ToArray();
        }

        private readonly IResourceManager _resources;

        private readonly Type _targetType;

        private readonly string _resourceName;

        private readonly IEnumerable<ScriptCommandParameter> _parameters;

        private static readonly CultureInfo _culture = new CultureInfo("en-US");
    }
}