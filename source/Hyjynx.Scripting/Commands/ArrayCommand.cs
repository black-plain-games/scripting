﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Commands
{
    public class ArrayCommand : IScriptCommand
    {
        public ScriptCommandType CommandType => ScriptCommandType.Array;

        public ArrayCommand(Type arrayType, IEnumerable<IScriptValue> values)
        {
            _arrayType = arrayType;

            _values = values;
        }

        public object GetValue()
        {
            var length = _values.Count();

            var output = Array.CreateInstance(_arrayType, length);

            for(var index = 0; index < length; index++)
            {
                var value = _values.ElementAt(index);

                output.SetValue(value.GetValue(), index);
            }

            return output;
        }

        private readonly IEnumerable<IScriptValue> _values;

        private readonly Type _arrayType;
    }
}