﻿namespace Hyjynx.Scripting.Commands
{
    public class RetrieveResourceCommand : IScriptCommand
    {
        public ScriptCommandType CommandType => ScriptCommandType.RetrieveResource;

        public RetrieveResourceCommand(string resourceName, IResourceManager resources)
        {
            _resourceName = resourceName;

            _resources = resources;
        }

        public object GetValue()
        {
            return _resources.Retrieve(_resourceName);
        }

        private readonly string _resourceName;

        private readonly IResourceManager _resources;
    }
}