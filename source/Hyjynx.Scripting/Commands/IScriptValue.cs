﻿namespace Hyjynx.Scripting
{
    public interface IScriptValue
    {
        object GetValue();
    }
}