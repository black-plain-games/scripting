﻿namespace Hyjynx.Scripting
{
    public class ScriptCommandParameter
    {
        public string ParameterName { get; set; }

        public IScriptValue Value { get; set; }
    }
}