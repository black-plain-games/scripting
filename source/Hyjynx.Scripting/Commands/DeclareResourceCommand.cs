﻿namespace Hyjynx.Scripting.Commands
{
    public class DeclareResourceCommand : IScriptCommand
    {
        public ScriptCommandType CommandType => ScriptCommandType.DeclareResource;

        public DeclareResourceCommand(IResourceManager resources, string identifier, IScriptValue value)
        {
            _resources = resources;

            _identifier = identifier;

            _value = value;
        }

        public object GetValue()
        {
            var value = _value.GetValue();

            _resources.Add(_identifier, _value.GetValue());

            return value;
        }

        private readonly IResourceManager _resources;

        private readonly string _identifier;

        private IScriptValue _value;
    }
}