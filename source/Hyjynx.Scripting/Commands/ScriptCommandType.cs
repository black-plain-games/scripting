﻿namespace Hyjynx.Scripting
{
    public enum ScriptCommandType
    {
        MakeResource,
        UnnameResource,
        CallMethod,
        ReadValue,
        SetValue,
        Null,
        Integer,
        Decimal,
        String,
        RequiredResource,
        RetrieveResource,
        Boolean,
        DeclareResource,
        Array
    }
}