﻿using System;

namespace Hyjynx.Scripting.Commands
{
    public class BooleanCommand : IScriptCommand
    {
        public ScriptCommandType CommandType => ScriptCommandType.Boolean;

        private BooleanCommand(bool value)
        {
            _value = value;
        }

        public object GetValue()
        {
            return _value;
        }

        public static BooleanCommand GetCommand(string value)
        {
            if(string.Compare("true", value, StringComparison.CurrentCultureIgnoreCase) == 0)
            {
                return _trueCommand;
            }
            else if(string.Compare("false", value, StringComparison.CurrentCultureIgnoreCase) == 0)
            {
                return _falseCommand;
            }
            else
            {
                throw new ArgumentOutOfRangeException(nameof(value));
            }
        }

        private readonly bool _value;

        private static readonly BooleanCommand _falseCommand = new BooleanCommand(false);

        private static readonly BooleanCommand _trueCommand = new BooleanCommand(true);
    }
}