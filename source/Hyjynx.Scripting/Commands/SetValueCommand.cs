﻿using System;
using System.Linq;

namespace Hyjynx.Scripting.Commands
{
    public class SetValueCommand : IScriptCommand
    {
        public ScriptCommandType CommandType => ScriptCommandType.SetValue;

        public SetValueCommand(IResourceManager resources, string resourceName, string propertyName, IScriptValue value)
        {
            _resources = resources;

            _resourceName = resourceName;

            _propertyName = propertyName;

            _value = value;
        }

        public object GetValue()
        {
            var resource = _resources.Retrieve(_resourceName);

            var targetType = resource.GetType();

            var properties = targetType.GetProperties();

            var property = properties.FirstOrDefault(p => string.Compare(p.Name, _propertyName, StringComparison.CurrentCultureIgnoreCase) == 0);

            object value = null;

            if(property != null)
            {
                value = _value.GetValue();

                property.SetValue(resource, value, null);

                return null;
            }

            var fields = targetType.GetFields();

            var field = fields.FirstOrDefault(f => string.Compare(f.Name, _propertyName, StringComparison.CurrentCultureIgnoreCase) == 0);

            if (field == null)
            {
                throw new ArgumentException($"No property or field named '{_propertyName}' exists on '{_resourceName}' of type '{targetType}'.");
            }

            value = _value.GetValue();

            field.SetValue(resource, value);

            return null;
        }

        private readonly IResourceManager _resources;

        private readonly string _resourceName;

        private readonly string _propertyName;

        private readonly IScriptValue _value;
    }
}