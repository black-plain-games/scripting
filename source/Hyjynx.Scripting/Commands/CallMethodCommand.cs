﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Hyjynx.Scripting.Exceptions;

namespace Hyjynx.Scripting.Commands
{
    public class CallMethodCommand : IScriptCommand
    {
        public ScriptCommandType CommandType => ScriptCommandType.CallMethod;

        public CallMethodCommand(IResourceManager resources, string resourceName, string methodName, IEnumerable<Type> genericTypeArguments, IEnumerable<ScriptCommandParameter> parameters)
        {
            _resources = resources;

            _resourceName = resourceName;

            _parameters = parameters;

            _methodName = methodName;

            _genericTypeArguments = genericTypeArguments ?? Enumerable.Empty<Type>();
        }

        public object GetValue()
        {
            var parameterValues = GetParameterValues();

            var resource = _resources.Retrieve(_resourceName);

            var methodInfo = GetMethod(resource, parameterValues);

            var parameters = GetParameterValues(methodInfo, parameterValues);
            
            return methodInfo.Invoke(resource, parameters);
        }

        private MethodInfo GetMethod(object target, IDictionary<string, object> parameterValues)
        {
            var targetType = target.GetType();

            var allMethods = targetType.GetMethods().ToList();

            var methods = targetType.GetMethods()
                                    .Where(mi => string.Compare(mi.Name, _methodName, StringComparison.CurrentCultureIgnoreCase) == 0);

            if(_genericTypeArguments.Any())
            {
                methods = methods.Where(mi => mi.ContainsGenericParameters)
                                 .Select(mi => mi.MakeGenericMethod(_genericTypeArguments.ToArray()));
            }

            foreach (var method in methods)
            {
                var parameters = new List<ParameterInfo>(method.GetParameters());

                var isMatch = true;

                foreach (var parameter in parameterValues)
                {
                    var cParameter = parameters.FirstOrDefault(p => string.Compare(p.Name, parameter.Key, StringComparison.CurrentCultureIgnoreCase) == 0);

                    if (cParameter == null)
                    {
                        isMatch = false;

                        break;
                    }

                    if (parameter.Value != null &&
                       !Utilities.CanConvertTo(cParameter.ParameterType, parameter.Value, _culture))
                    {
                        isMatch = false;

                        break;
                    }

                    parameters.Remove(cParameter);
                }

                if (isMatch && parameters.Any())
                {
                    foreach (var parameter in parameters)
                    {
                        if (!parameter.IsOptional)
                        {
                            isMatch = false;

                            break;
                        }
                    }
                }

                if(isMatch && _genericTypeArguments.Any())
                {
                    var genericMethod = method.GetGenericMethodDefinition();

                    return genericMethod.MakeGenericMethod(_genericTypeArguments.ToArray());
                }

                if (isMatch)
                {
                    return method;
                }
            }

            throw new InvalidParameterListException(targetType, _parameters);
        }

        private IDictionary<string, object> GetParameterValues()
        {
            var output = new Dictionary<string, object>();

            foreach (var parameter in _parameters)
            {
                output.Add(parameter.ParameterName, parameter.Value.GetValue());
            }

            return output;
        }

        private object[] GetParameterValues(MethodInfo method, IDictionary<string, object> parameters)
        {
            var output = new List<object>();

            var methodParameters = method.GetParameters();

            foreach (var methodParameter in methodParameters)
            {
                var parameter = parameters.FirstOrDefault(p => string.Compare(methodParameter.Name, p.Key, StringComparison.CurrentCultureIgnoreCase) == 0);

                object parameterValue = null;

                if (parameter.Key != null)
                {
                    if (parameter.Value == null)
                    {
                        parameterValue = null;
                    }
                    else if (methodParameter.ParameterType.IsAssignableFrom(parameter.Value.GetType()))
                    {
                        parameterValue = parameter.Value;
                    }
                    else
                    {
                        parameterValue = Convert.ChangeType(parameter.Value, methodParameter.ParameterType, _culture);
                    }
                }
                else
                {
                    parameterValue = methodParameter.DefaultValue;
                }

                output.Add(parameterValue);
            }

            return output.ToArray();
        }

        private readonly IResourceManager _resources;

        private readonly string _resourceName;

        private readonly string _methodName;

        private readonly IEnumerable<ScriptCommandParameter> _parameters;

        private readonly IEnumerable<Type> _genericTypeArguments;

        private static readonly CultureInfo _culture = new CultureInfo("en-US");
    }
}