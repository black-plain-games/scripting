﻿namespace Hyjynx.Scripting
{
    public interface IResourceManager
    {
        void Add(string identifier, object value);

        void Add(string identifier, object value, bool canRemove);

        void Remove(string identifier);

        object Retrieve(string identifier);

        bool Contains(string identifier);
    }
}