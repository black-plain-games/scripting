﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    public sealed class TokenIdentifierContainer
    {
        public TokenIdentifierContainer()
        {
            _identifiers = new LinkedList<ITokenIdentifier>();

            Initialize();
        }

        public void RegisterTokenIdentifier<TIdentifier>()
            where TIdentifier : ITokenIdentifier, new()
        {
            _identifiers.Add(new TIdentifier());
        }

        internal ScriptTokenType GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            var tokenType = TryGetTokenType(token, tokens, _identifiers);

            if(tokenType.HasValue)
            {
                return tokenType.Value;
            }

            return ScriptTokenType.Unknown;
        }

        private ScriptTokenType? TryGetTokenType(string token, IEnumerable<ScriptToken> tokens, IEnumerable<ITokenIdentifier> identifiers)
        {
            foreach (var identifier in identifiers)
            {
                var eagerIdentifiers = GetTokenIdentifiers(identifier.RunAfter);

                var result = TryGetTokenType(token, tokens, eagerIdentifiers);

                if (result.HasValue)
                {
                    return result.Value;
                }

                result = identifier.GetTokenType(token, tokens);

                if (result.HasValue)
                {
                    return result.Value;
                }
            }

            return null;
        }

        private IEnumerable<ITokenIdentifier> GetTokenIdentifiers(IEnumerable<Type> identifierTypes)
        {
            var output = new List<ITokenIdentifier>();

            foreach(var identifierType in identifierTypes)
            {
                var identifier = _identifiers.FirstOrDefault(i => i.GetType() == identifierType);

                if(identifier == null)
                {
                    // TODO: strongly type this argument
                    throw new Exception("Identifier type not registered.");
                }

                output.Add(identifier);
            }

            return output;
        }

        private void Initialize()
        {
            Helpers.LoadInstancesFromCurrentAssembly(_identifiers);
        }

        private readonly ICollection<ITokenIdentifier> _identifiers;
    }
}