﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class ParameterListTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if (token == PARAMETER_LIST_TOKEN)
            {
                return ScriptTokenType.ParameterList;
            }

            return null;
        }

        private static readonly string PARAMETER_LIST_TOKEN = "with";
    }
}