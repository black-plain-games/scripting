﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class MethodNameTokenIdentifier : ITokenIdentifier
    {
        IEnumerable<Type> ITokenIdentifier.RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            var previous = tokens.LastOrDefault();

            if(previous!= null && previous.TokenType == ScriptTokenType.CallMethod)
            {
                return ScriptTokenType.MethodName;
            }

            return null;
        }
    }
}