﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class DeclareResourceTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if(token == DECLARE_RESOURCE_TOKEN)
            {
                return ScriptTokenType.DeclareResource;
            }

            return null;
        }

        private static readonly string DECLARE_RESOURCE_TOKEN = "declare";
    }
}