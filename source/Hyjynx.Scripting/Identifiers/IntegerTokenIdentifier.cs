﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Hyjynx.Scripting.Identifiers
{
    internal class IntegerTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if (_integerExpression.IsMatch(token))
            {
                return ScriptTokenType.Integer;
            }

            return null;
        }

        private static readonly Regex _integerExpression = new Regex("^-?[0-9]*$");
    }
}