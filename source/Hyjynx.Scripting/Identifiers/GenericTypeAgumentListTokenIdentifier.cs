﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class GenericTypeAgumentListTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if(token == GENERIC_TYPE_ARGMUMENT_TOKEN)
            {
                var previous = tokens.LastOrDefault();

                if(previous != null && (previous.TokenType == ScriptTokenType.MethodName || previous.TokenType == ScriptTokenType.ArrayEnd))
                {
                    return ScriptTokenType.GenericTypeArgumentList;
                }
            }

            return null;
        }

        private static readonly string GENERIC_TYPE_ARGMUMENT_TOKEN = "of";
    }
}