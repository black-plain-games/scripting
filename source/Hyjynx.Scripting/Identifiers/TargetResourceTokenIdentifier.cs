﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    public class TargetResourceTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            var previous = tokens.LastOrDefault();

            if(previous != null && token == TARGET_RESOURCE_TOKEN &&
                (previous.TokenType == ScriptTokenType.PropertyName ||
                 previous.TokenType == ScriptTokenType.MethodName ||
                 previous.TokenType == ScriptTokenType.GenericTypeArgument))
            {
                return ScriptTokenType.TargetResource;
            }

            return null;
        }

        private static readonly string TARGET_RESOURCE_TOKEN = "on";
    }
}