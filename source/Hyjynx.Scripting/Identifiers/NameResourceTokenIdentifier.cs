﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class NameResourceTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if (token == NAME_RESOURCE_TOKEN)
            {
                return ScriptTokenType.NameResource;
            }

            return null;
        }

        private static readonly string NAME_RESOURCE_TOKEN = "named";
    }
}