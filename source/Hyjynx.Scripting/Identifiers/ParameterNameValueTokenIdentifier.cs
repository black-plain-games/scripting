﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class ParameterNameValueTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => new [] { typeof(ResourceValueTokenIdentifier) };

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            var previousToken = tokens.LastOrDefault();

            if(token == AS_TOKEN)
            {
                if (previousToken.TokenType == ScriptTokenType.Unknown)
                {
                    previousToken.TokenType = ScriptTokenType.ParameterName;
                }

                return ScriptTokenType.ParameterValue;
            }

            return null;
        }

        private static readonly string AS_TOKEN = "as";
    }
}