﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class UnnameResourceTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if(token == UNNAME_RESOURCE_TOKEN)
            {
                return ScriptTokenType.UnnameResource;
            }

            return null;
        }

        private static readonly string UNNAME_RESOURCE_TOKEN = "unname";
    }
}