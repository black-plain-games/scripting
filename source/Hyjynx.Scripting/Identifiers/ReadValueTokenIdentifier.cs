﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class ReadValueTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if(token == READ_VALUE_TOKEN)
            {
                return ScriptTokenType.ReadValue;
            }

            return null;
        }

        private static readonly string READ_VALUE_TOKEN = "read";
    }
}