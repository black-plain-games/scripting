﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class PropertyNameTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            var previous = tokens.LastOrDefault();

            if(previous != null &&
                (previous.TokenType == ScriptTokenType.SetValue || previous.TokenType == ScriptTokenType.ReadValue))
            {
                return ScriptTokenType.PropertyName;
            }

            return null;
        }
    }
}