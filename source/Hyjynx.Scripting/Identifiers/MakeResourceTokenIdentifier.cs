﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class MakeResourceTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if (token == MAKE_RESOURCE_TOKEN)
            {
                return ScriptTokenType.MakeResource;
            }

            return null;
        }

        private static readonly string MAKE_RESOURCE_TOKEN = "make";
    }
}