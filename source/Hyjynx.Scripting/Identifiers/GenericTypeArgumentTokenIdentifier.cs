﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    public class GenericTypeArgumentTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => new[]
        {
            typeof(TargetResourceTokenIdentifier)
        };

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            var previous = tokens.LastOrDefault();

            if(previous != null &&
                (previous.TokenType == ScriptTokenType.GenericTypeArgument ||
                previous.TokenType == ScriptTokenType.GenericTypeArgumentList))
            {
                return ScriptTokenType.GenericTypeArgument;
            }

            return null;
        }
    }
}