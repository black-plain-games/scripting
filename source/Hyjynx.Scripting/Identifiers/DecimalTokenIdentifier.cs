﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Hyjynx.Scripting.Identifiers
{
    internal class DecimalTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if (_decimalExpression.IsMatch(token))
            {
                return ScriptTokenType.Decimal;
            }

            return null;
        }

        private static readonly Regex _decimalExpression = new Regex(@"^-?[0-9]*\.[0-9]*$");
    }
}