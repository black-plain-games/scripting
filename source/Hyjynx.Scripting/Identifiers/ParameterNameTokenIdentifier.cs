﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class ParameterNameTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            var previousToken = tokens.LastOrDefault();

            if(previousToken != null &&
                previousToken.TokenType == ScriptTokenType.ParameterList)
            {
                return ScriptTokenType.ParameterName;
            }

            return null;
        }
    }
}