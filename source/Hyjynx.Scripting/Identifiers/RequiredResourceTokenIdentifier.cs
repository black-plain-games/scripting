﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class RequiredResourceTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if (token == REQUIRED_RESOURCE_TOKEN)
            {
                return ScriptTokenType.RequiredResource;
            }

            return null;
        }

        private static readonly string REQUIRED_RESOURCE_TOKEN = "requires";
    }
}