﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class ResourceValueTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            var previous = tokens.LastOrDefault();

            if(previous != null &&
               previous.TokenType == ScriptTokenType.String &&
               token == RESOURCE_VALUE_TOKEN)
            {
                return ScriptTokenType.ResourceValue;
            }

            return null;
        }

        private static readonly string RESOURCE_VALUE_TOKEN = "as";
    }
}