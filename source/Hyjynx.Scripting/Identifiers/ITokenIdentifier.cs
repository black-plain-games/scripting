﻿using System;
using System.Collections.Generic;

namespace Hyjynx.Scripting.Identifiers
{
    public interface ITokenIdentifier
    {
        IEnumerable<Type> RunAfter { get; }

        ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens);
    }
}