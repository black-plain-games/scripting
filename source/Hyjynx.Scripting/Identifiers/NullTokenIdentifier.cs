﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class NullTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if (token == NULL_TOKEN)
            {
                return ScriptTokenType.Null;
            }

            return null;
        }

        private static readonly string NULL_TOKEN = "null";
    }
}