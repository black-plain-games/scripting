﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class BooleanTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if(token == TRUE_TOKEN ||
               token == FALSE_TOKEN)
            {
                return ScriptTokenType.Boolean;
            }

            return null;
        }

        private static readonly string TRUE_TOKEN = "true";

        private static readonly string FALSE_TOKEN = "false";
    }
}