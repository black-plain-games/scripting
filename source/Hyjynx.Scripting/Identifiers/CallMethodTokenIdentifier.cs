﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class CallMethodTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if(token == CALL_TOKEN)
            {
                return ScriptTokenType.CallMethod;
            }

            return null;
        }

        private static readonly string CALL_TOKEN = "call";
    }
}