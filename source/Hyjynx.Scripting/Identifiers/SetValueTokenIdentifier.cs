﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class SetValueTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if(token == SET_VALUE_TOKEN)
            {
                return ScriptTokenType.SetValue;
            }

            return null;
        }

        private static readonly string SET_VALUE_TOKEN = "set";
    }
}