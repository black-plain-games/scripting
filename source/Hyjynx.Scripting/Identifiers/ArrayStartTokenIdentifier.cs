﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class ArrayStartTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if(token == ARRAY_START_TOKEN)
            {
                return ScriptTokenType.ArrayStart;
            }

            return null;
        }

        private static readonly string ARRAY_START_TOKEN = "(";
    }
}