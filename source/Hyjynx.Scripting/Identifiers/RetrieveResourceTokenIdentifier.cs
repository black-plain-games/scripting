﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class RetrieveResourceTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if (token.StartsWith("[", StringComparison.CurrentCulture) && token.EndsWith("]", StringComparison.CurrentCulture))
            {
                return ScriptTokenType.RetrieveResource;
            }

            return null;
        }
    }
}