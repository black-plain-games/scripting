﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class ArrayEndTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if (token == ARRAY_END_TOKEN)
            {
                return ScriptTokenType.ArrayEnd;
            }

            return null;
        }

        private static readonly string ARRAY_END_TOKEN = ")";
    }
}