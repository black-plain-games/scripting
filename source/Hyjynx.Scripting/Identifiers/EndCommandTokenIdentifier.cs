﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Identifiers
{
    internal class EndCommandTokenIdentifier : ITokenIdentifier
    {
        public IEnumerable<Type> RunAfter => Enumerable.Empty<Type>();

        public ScriptTokenType? GetTokenType(string token, IEnumerable<ScriptToken> tokens)
        {
            if (token == END_COMMAND_TOKEN)
            {
                return ScriptTokenType.EndCommand;
            }

            return null;
        }

        private static readonly string END_COMMAND_TOKEN = "go";
    }
}