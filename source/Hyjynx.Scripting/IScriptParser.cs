﻿namespace Hyjynx.Scripting
{
    public interface IScriptParser
    {
        Script Parse(string scriptContent);
    }
}