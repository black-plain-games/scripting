﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Hyjynx.Scripting
{
    internal static class Helpers
    {
        public static void LoadInstancesFromCurrentAssembly<T>(ICollection<T> target)
            where T : class
        {
            var scriptingAssembly = Assembly.GetExecutingAssembly();

            var types = scriptingAssembly.GetTypes().Where(t => !t.IsAbstract && t.GetInterfaces().Contains(typeof(T)));

            foreach (var type in types)
            {
                var instance = Activator.CreateInstance(type) as T;

                target.Add(instance);
            }
        }
    }
}