﻿using System;

namespace Hyjynx.Scripting
{
    public interface ITypeEvaluator
    {
        Type GetType(string name);

        Type GetType(string startsWith, string endsWith);
    }
}