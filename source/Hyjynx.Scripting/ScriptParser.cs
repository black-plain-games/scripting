﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Hyjynx.Scripting.Exceptions;
using Hyjynx.Scripting.Generators;
using Hyjynx.Scripting.Identifiers;

namespace Hyjynx.Scripting
{
    public class ScriptParser : IScriptParser
    {
        public ScriptParser(CommandGeneratorContainer commandGenerators, TokenIdentifierContainer tokenIdentifiers)
        {
            _commandGenerators = commandGenerators;

            _tokenIdentifiers = tokenIdentifiers;
        }

        public Script Parse(string scriptContent)
        {
            var script = new Script();

            try
            {
                var tokens = ParseTokens(scriptContent);

                var tokenCount = tokens.Count();

                var tokenIndex = 0;

                while(tokenIndex < tokenCount)
                {
                    var token = tokens.ElementAt(tokenIndex);

                    if (token.TokenType == ScriptTokenType.EndCommand)
                    {
                        tokenIndex++;

                        continue;
                    }

                    IScriptCommand command = null;

                    var commandGenerator = _commandGenerators.GetCommandGenerator(token.TokenType);

                    if (commandGenerator == null)
                    {
                        throw new UnexpectedTokenException(token, tokenIndex);
                    }

                    command = commandGenerator.Generate(tokens, ref tokenIndex);

                    if (command == null)
                    {
                        break;
                    }

                    script.Commands.Add(command);
                }

            }
            catch(Exception err)
            {
                script.ErrorMessage = err.Message;
            }

            return script;
        }

        private IEnumerable<ScriptToken> ParseTokens(string scriptContent)
        {
            var tokens = new List<ScriptToken>();

            using (var reader = new StringReader(scriptContent))
            {
                var tokenBuilder = new StringBuilder();

                int currentCharacter = -1;

                var includeWhiteSpacesInToken = false;

                var isCharacterEscaped = false;

                while ((currentCharacter = reader.Read()) > 0)
                {
                    if(!includeWhiteSpacesInToken)
                    {
                        currentCharacter = char.ToLower((char)currentCharacter);

                        if (IsWhiteSpace(currentCharacter))
                        {
                            TrySaveToken(tokenBuilder, tokens);

                            continue;
                        }
                    }

                    if (IsWhiteSpaceToggleCharacter(currentCharacter))
                    {
                        if (!isCharacterEscaped)
                        {
                            includeWhiteSpacesInToken = !includeWhiteSpacesInToken;
                        }
                    }

                    if(isCharacterEscaped)
                    {
                        isCharacterEscaped = false;
                    }
                    else if(IsEscapeCharacter(currentCharacter))
                    {
                        isCharacterEscaped = true;

                        continue;
                    }

                    tokenBuilder.Append((char)currentCharacter);
                }
                
                TrySaveToken(tokenBuilder, tokens);
            }

            return tokens;
        }

        private void TrySaveToken(StringBuilder tokenBuilder, ICollection<ScriptToken> tokens)
        {
            if (tokenBuilder.Length > 0)
            {
                var tokenValue = tokenBuilder.ToString();

                var tokenType = _tokenIdentifiers.GetTokenType(tokenValue, tokens);

                if(tokenType == ScriptTokenType.String ||
                   tokenType == ScriptTokenType.RetrieveResource)
                {
                    tokenValue = tokenValue.Substring(1, tokenValue.Length - 2);
                }

                tokens.Add(new ScriptToken
                {
                    TokenType = tokenType,
                    Value = tokenValue
                });

                tokenBuilder.Clear();
            }
        }

        private bool IsWhiteSpace(int character)
        {
            return char.IsWhiteSpace((char)character);
        }

        private bool IsWhiteSpaceToggleCharacter(int character)
        {
            return character == '"' || character == '[' || character == ']';
        }

        private bool IsEscapeCharacter(int character)
        {
            return character == '\\';
        }

        private readonly CommandGeneratorContainer _commandGenerators;

        private readonly TokenIdentifierContainer _tokenIdentifiers;
    }
}