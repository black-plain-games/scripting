﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hyjynx.Scripting.Commands;
using Hyjynx.Scripting.Exceptions;

namespace Hyjynx.Scripting.Generators
{
    public class SetValueCommandGenerator : BaseCommandGenerator
    {
        public override bool CanHandleToken(ScriptTokenType tokenType)
        {
            return tokenType == ScriptTokenType.SetValue;
        }

        public override IScriptCommand Generate(IEnumerable<ScriptToken> tokens, ref int index)
        {
            var token = tokens.ElementAt(++index);

            if(token.TokenType != ScriptTokenType.PropertyName)
            {
                throw new UnexpectedTokenException(token, index);
            }

            var propertyName = token.Value;

            token = tokens.ElementAt(++index);

            if(token.TokenType != ScriptTokenType.TargetResource)
            {
                throw new UnexpectedTokenException(token, index);
            }

            token = tokens.ElementAt(++index);

            if(token.TokenType != ScriptTokenType.RetrieveResource)
            {
                throw new UnexpectedTokenException(token, index);
            }

            var resourceName = token.Value;

            token = tokens.ElementAtOrDefault(++index);

            if(token.TokenType != ScriptTokenType.ParameterValue)
            {
                throw new UnexpectedTokenException(token, index);
            }

            token = tokens.ElementAtOrDefault(++index);

            var generator = GetCommandGenerator(token.TokenType);

            var value = generator.Generate(tokens, ref index);

            return new SetValueCommand(Resources, resourceName, propertyName, value);
        }
    }
}