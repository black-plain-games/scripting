﻿using System.Collections.Generic;
using System.Linq;
using Hyjynx.Scripting.Exceptions;

namespace Hyjynx.Scripting.Generators
{
    public class NullCommandGenerator : BaseCommandGenerator
    {
        public override bool CanHandleToken(ScriptTokenType tokenType)
        {
            return tokenType == ScriptTokenType.Null;
        }

        public override IScriptCommand Generate(IEnumerable<ScriptToken> tokens, ref int index)
        {
            var value = tokens.ElementAt(index++);

            if(value.Value == "null")
            {
                return NullCommand.Instance;
            }

            throw new UnexpectedTokenException(value, index);
        }
    }
}