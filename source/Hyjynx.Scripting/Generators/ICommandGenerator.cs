﻿using System.Collections.Generic;

namespace Hyjynx.Scripting.Generators
{
    public interface ICommandGenerator
    {
        void Initialize(IResourceManager resourceManager, ITypeEvaluator typeEvaluator, CommandGeneratorContainer commandGeneratorContainer);

        bool CanHandleToken(ScriptTokenType tokenType);

        IScriptCommand Generate(IEnumerable<ScriptToken> tokens, ref int index);
    }
}