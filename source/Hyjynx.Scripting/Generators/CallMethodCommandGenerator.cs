﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hyjynx.Scripting.Commands;
using Hyjynx.Scripting.Exceptions;

namespace Hyjynx.Scripting.Generators
{
    public class CallMethodCommandGenerator : BaseCommandGenerator
    {
        public override bool CanHandleToken(ScriptTokenType tokenType)
        {
            return tokenType == ScriptTokenType.CallMethod;
        }

        public override IScriptCommand Generate(IEnumerable<ScriptToken> tokens, ref int index)
        {
            var token = tokens.ElementAt(++index);

            if (token.TokenType != ScriptTokenType.MethodName)
            {
                throw new UnexpectedTokenException(token, index);
            }

            var methodName = token.Value;

            IEnumerable<Type> genericArguments = null;

            token = tokens.ElementAt(++index);

            if (token.TokenType == ScriptTokenType.GenericTypeArgumentList)
            {
                genericArguments = GetGenericTypeArguments(tokens, ref index);
            }

            token = tokens.ElementAt(index);

            if (token.TokenType != ScriptTokenType.TargetResource)
            {
                throw new UnexpectedTokenException(token, index);
            }

            token = tokens.ElementAt(++index);

            if(token.TokenType != ScriptTokenType.RetrieveResource)
            {
                throw new UnexpectedTokenException(token, index);
            }

            var resourceName = token.Value;

            var parameters = Enumerable.Empty<ScriptCommandParameter>();

            token = tokens.ElementAt(++index);

            if (token.TokenType == ScriptTokenType.ParameterList)
            {
                parameters = GetParameters(tokens, ref index);

                token = tokens.ElementAt(index);
            }

            if (token.TokenType == ScriptTokenType.EndCommand)
            {
                ++index;

                return new CallMethodCommand(Resources, resourceName, methodName, genericArguments, parameters);
            }

            throw new UnexpectedTokenException(token, index);
        }

        private ScriptToken GetName(IEnumerable<ScriptToken> tokens, ref int index)
        {
            return tokens.ElementAt(index++);
        }

        private IEnumerable<ScriptCommandParameter> GetParameters(IEnumerable<ScriptToken> tokens, ref int index)
        {
            var output = new List<ScriptCommandParameter>();

            var token = tokens.ElementAt(index++);

            while (token.TokenType != ScriptTokenType.EndCommand)
            {
                var parameter = GetParameter(tokens, ref index);

                output.Add(parameter);

                token = tokens.ElementAt(index);
            }

            return output;
        }

        private ScriptCommandParameter GetParameter(IEnumerable<ScriptToken> tokens, ref int index)
        {
            var token = tokens.ElementAt(index++);

            if (token.TokenType != ScriptTokenType.ParameterName)
            {
                throw new UnexpectedTokenException(token, index);
            }

            var output = new ScriptCommandParameter
            {
                ParameterName = token.Value
            };

            token = tokens.ElementAt(index++);

            if (token.TokenType != ScriptTokenType.ParameterValue)
            {
                throw new UnexpectedTokenException(token, index);
            }

            token = tokens.ElementAt(index);

            var commandGenerator = GetCommandGenerator(token.TokenType);

            output.Value = commandGenerator.Generate(tokens, ref index);

            return output;
        }

        private IEnumerable<Type> GetGenericTypeArguments(IEnumerable<ScriptToken> tokens, ref int index)
        {
            var output = new List<Type>();

            var token = tokens.ElementAt(++index);

            while(token.TokenType == ScriptTokenType.GenericTypeArgument)
            {
                var type = GetType(token.Value);

                if(output == null)
                {
                    throw new UnknownTypeException(token.Value, index);
                }

                output.Add(type);

                token = tokens.ElementAt(++index);
            }

            return output;
        }
    }
}