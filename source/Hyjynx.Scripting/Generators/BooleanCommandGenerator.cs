﻿using System.Collections.Generic;
using System.Linq;
using Hyjynx.Scripting.Commands;

namespace Hyjynx.Scripting.Generators
{
    public class BooleanCommandGenerator : BaseCommandGenerator
    {
        public override bool CanHandleToken(ScriptTokenType tokenType)
        {
            return tokenType == ScriptTokenType.Boolean;
        }

        public override IScriptCommand Generate(IEnumerable<ScriptToken> tokens, ref int index)
        {
            var token = tokens.ElementAt(index++);

            return BooleanCommand.GetCommand(token.Value);
        }
    }
}