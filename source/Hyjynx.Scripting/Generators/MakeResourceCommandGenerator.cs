﻿using System.Collections.Generic;
using System.Linq;
using Hyjynx.Scripting.Commands;
using Hyjynx.Scripting.Exceptions;

namespace Hyjynx.Scripting.Generators
{
    public class MakeResourceCommandGenerator : BaseCommandGenerator
    {
        public override bool CanHandleToken(ScriptTokenType tokenType)
        {
            return tokenType == ScriptTokenType.MakeResource;
        }

        public override IScriptCommand Generate(IEnumerable<ScriptToken> tokens, ref int index)
        {
            index++;

            var token = tokens.ElementAt(index);

            var type = GetType(token.Value);

            if (type == null)
            {
                throw new UnknownTypeException(token.Value, index);
            }

            index++;

            var targetType = type;

            token = tokens.ElementAt(index);

            string resourceName = null;

            if (token.TokenType == ScriptTokenType.NameResource)
            {
                index++;

                var nameToken = GetName(tokens, ref index);

                if (nameToken.TokenType != ScriptTokenType.String)
                {
                    throw new UnexpectedTokenException(token, index);
                }

                resourceName = nameToken.Value;

                token = tokens.ElementAt(index);
            }

            var parameters = Enumerable.Empty<ScriptCommandParameter>();

            if (token.TokenType == ScriptTokenType.ParameterList)
            {
                parameters = GetParameters(tokens, ref index);

                token = tokens.ElementAt(index);
            }

            if (token.TokenType == ScriptTokenType.EndCommand)
            {
                ++index;

                return new MakeResourceCommand(Resources, targetType, resourceName, parameters);
            }

            throw new UnexpectedTokenException(token, index);
        }
        
        private ScriptToken GetName(IEnumerable<ScriptToken> tokens, ref int index)
        {
            return tokens.ElementAt(index++);
        }

        private IEnumerable<ScriptCommandParameter> GetParameters(IEnumerable<ScriptToken> tokens, ref int index)
        {
            var output = new List<ScriptCommandParameter>();

            var token = tokens.ElementAt(index++);

            while (token.TokenType != ScriptTokenType.EndCommand)
            {
                var parameter = GetParameter(tokens, ref index);

                output.Add(parameter);

                token = tokens.ElementAt(index);
            }

            return output;
        }

        private ScriptCommandParameter GetParameter(IEnumerable<ScriptToken> tokens, ref int index)
        {
            var token = tokens.ElementAt(index++);

            if(token.TokenType != ScriptTokenType.ParameterName)
            {
                throw new UnexpectedTokenException(token, index);
            }

            var output = new ScriptCommandParameter
            {
                ParameterName = token.Value
            };

            token = tokens.ElementAt(index++);

            if (token.TokenType != ScriptTokenType.ParameterValue)
            {
                throw new UnexpectedTokenException(token, index);
            }

            token = tokens.ElementAt(index);

            var commandGenerator = GetCommandGenerator(token.TokenType);

            output.Value = commandGenerator.Generate(tokens, ref index);

            return output;
        }
    }
}