﻿using System;
using System.Collections.Generic;

namespace Hyjynx.Scripting.Generators
{
    public abstract class BaseCommandGenerator : ICommandGenerator
    {
        public abstract bool CanHandleToken(ScriptTokenType tokenType);

        public abstract IScriptCommand Generate(IEnumerable<ScriptToken> tokens, ref int index);

        public void Initialize(IResourceManager resourceManager, ITypeEvaluator typeEvaluator, CommandGeneratorContainer commandGeneratorContainer)
        {
            _resources = resourceManager;

            _types = typeEvaluator;

            _commandGenerators = commandGeneratorContainer;
        }

        protected Type GetType(string typeName)
        {
            var typeNameParts = typeName.Split(':');

            if(typeNameParts.Length == 1)
            {
                return _types.GetType(typeNameParts[0]);
            }
            else if(typeNameParts.Length == 2)
            {
                return _types.GetType(typeNameParts[0], typeNameParts[1]);
            }

            throw new ArgumentOutOfRangeException(nameof(typeName));
        }

        protected ICommandGenerator GetCommandGenerator(ScriptTokenType tokenType)
        {
            return _commandGenerators.GetCommandGenerator(tokenType);
        }

        protected IResourceManager Resources
        {
            get
            {
                return _resources;
            }
        }

        private IResourceManager _resources;

        private ITypeEvaluator _types;

        private CommandGeneratorContainer _commandGenerators;
    }
}