﻿using System.Collections.Generic;
using System.Linq;
using Hyjynx.Scripting.Commands;
using Hyjynx.Scripting.Exceptions;

namespace Hyjynx.Scripting.Generators
{
    public class DeclareResourceCommandGenerator : BaseCommandGenerator
    {
        public override bool CanHandleToken(ScriptTokenType tokenType)
        {
            return tokenType == ScriptTokenType.DeclareResource;
        }

        public override IScriptCommand Generate(IEnumerable<ScriptToken> tokens, ref int index)
        {
            index++;

            var token = tokens.ElementAt(index);

            if(token.TokenType != ScriptTokenType.String)
            {
                throw new UnexpectedTokenException(token, index);
            }

            var resourceName = token.Value;

            token = tokens.ElementAt(++index);

            if (token.TokenType == ScriptTokenType.ResourceValue)
            {
                index++;

                token = tokens.ElementAt(index);

                var commandGenerator = GetCommandGenerator(token.TokenType);

                var value = commandGenerator.Generate(tokens, ref index);

                return new DeclareResourceCommand(Resources, resourceName, value);
            }

            throw new UnexpectedTokenException(token, index);
        }
    }
}