﻿using System.Collections.Generic;
using System.Linq;

namespace Hyjynx.Scripting.Generators
{
    public sealed class CommandGeneratorContainer
    {
        public CommandGeneratorContainer(IResourceManager resources, ITypeEvaluator typeEvaluator)
        {
            _resources = resources;

            _types = typeEvaluator;

            _commandGenerators = new LinkedList<ICommandGenerator>();

            Initialize();
        }

        public void RegisterGenerator<TCommandGenerator>()
            where TCommandGenerator : ICommandGenerator, new()
        {
            var commandGenerator = new TCommandGenerator();

            commandGenerator.Initialize(_resources, _types, this);

            _commandGenerators.Add(commandGenerator);
        }

        internal ICommandGenerator GetCommandGenerator(ScriptTokenType tokenType)
        {
            return _commandGenerators.FirstOrDefault(cg => cg.CanHandleToken(tokenType));
        }

        private void Initialize()
        {
            Helpers.LoadInstancesFromCurrentAssembly(_commandGenerators);

            foreach(var commandGenerator in _commandGenerators)
            {
                var baseCommandGenerator = commandGenerator as BaseCommandGenerator;

                if(baseCommandGenerator != null)
                {
                    baseCommandGenerator.Initialize(_resources, _types, this);
                }
            }
        }

        private readonly IResourceManager _resources;

        private readonly ITypeEvaluator _types;

        private readonly ICollection<ICommandGenerator> _commandGenerators;
    }
}