﻿using System.Collections.Generic;
using System.Linq;
using Hyjynx.Scripting.Commands;

namespace Hyjynx.Scripting.Generators
{
    public class UnnameResourceCommandGenerator : BaseCommandGenerator
    {
        public override bool CanHandleToken(ScriptTokenType tokenType)
        {
            return tokenType == ScriptTokenType.UnnameResource;
        }

        public override IScriptCommand Generate(IEnumerable<ScriptToken> tokens, ref int index)
        {
            var token = tokens.ElementAt(++index);

            index++;

            return new UnnameResourceCommand(Resources, token.Value);
        }
    }
}