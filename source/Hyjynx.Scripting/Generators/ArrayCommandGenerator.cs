﻿using System;
using System.Collections.Generic;
using System.Linq;
using Hyjynx.Scripting.Commands;
using Hyjynx.Scripting.Exceptions;

namespace Hyjynx.Scripting.Generators
{
    public class ArrayCommandGenerator : BaseCommandGenerator
    {
        public override bool CanHandleToken(ScriptTokenType tokenType)
        {
            return tokenType == ScriptTokenType.ArrayStart;
        }

        public override IScriptCommand Generate(IEnumerable<ScriptToken> tokens, ref int index)
        {
            var values = new List<IScriptCommand>();

            var token = tokens.ElementAt(++index);

            while(token.TokenType != ScriptTokenType.ArrayEnd)
            {
                var generator = GetCommandGenerator(token.TokenType);

                var value = generator.Generate(tokens, ref index);

                values.Add(value);

                token = tokens.ElementAt(index);
            }

            token = tokens.ElementAt(++index);

            if (token.TokenType != ScriptTokenType.GenericTypeArgumentList)
            {
                throw new UnexpectedTokenException(token, index);
            }

            token = tokens.ElementAt(++index);

            if(token.TokenType != ScriptTokenType.GenericTypeArgument)
            {
                throw new UnexpectedTokenException(token, index);
            }

            var type = GetType(token.Value);

            ++index;

            return new ArrayCommand(type, values);
        }
    }
}