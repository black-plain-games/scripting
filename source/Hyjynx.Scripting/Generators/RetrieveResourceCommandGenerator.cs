﻿using System.Collections.Generic;
using System.Linq;
using Hyjynx.Scripting.Commands;
using Hyjynx.Scripting.Exceptions;

namespace Hyjynx.Scripting.Generators
{
    public class RetrieveResourceCommandGenerator : BaseCommandGenerator
    {
        public override bool CanHandleToken(ScriptTokenType tokenType)
        {
            return tokenType == ScriptTokenType.RetrieveResource;
        }

        public override IScriptCommand Generate(IEnumerable<ScriptToken> tokens, ref int index)
        {
            var token = tokens.ElementAt(index++);

            if(token.TokenType != ScriptTokenType.RetrieveResource)
            {
                throw new UnexpectedTokenException(token, index);
            }

            return new RetrieveResourceCommand(token.Value, Resources);
        }
    }
}