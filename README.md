# scripting
Scripting is a Hyjynx Studios custom language parser. Below is a sample of the language. Note: not all of the feature listed are supported.

	REQUIRES [SampleTexture]

	MAKE Texture2DComponent NAMED "TestSpriteTexture" WITH
		texture AS [SampleTexture]
	GO

	MAKE TranslationComponent NAMED "TestSpriteTranslation" WITH
		position AS
			MAKE Vector2 WITH
				x AS 500.0
				y AS 500.0
			GO
	GO

	MAKE Sprite2D NAMED "TestSprite" WITH
		texture AS [TestSpriteTexture]
		translation AS [TestSpriteTranslation]
	GO
		
	UNNAME [TestSpriteTexture] GO

	MAKE ScaleComponent NAMED "TestSpriteScale" WITH
		scale AS
			MAKE Vector2 WITH
				x AS 5
				y AS 5
			GO
	GO
						
	CALL SetComponent OF IScaleComponent ON [TestSprite] WITH
		component AS [TestSpriteScale]
	GO
		
	MAKE AnimationComponent NAMED "TestSpriteAnimation" WITH
		setup AS
			MAKE BasicAnimationComponentSetup WITH
				dimensions AS
					MAKE Vector2 WITH
						x AS 200
						y AS 150
					GO
				columnCount AS 4
				frameCount AS 9
				timerPeriod AS 1000
				lastFrameIndex AS 8
			GO
	GO

	CALL SetComponent OF ISourceComponent ON [TestSprite] WITH
		component AS [TestSpriteAnimation]
	GO

	MAKE SpriteBoundsComponent NAMED "TestSpriteBounds" WITH
		translation AS [TestSpriteTranslation]
		scale AS [TestSpriteScale]
		source AS [TestSpriteAnimation]
	GO
						
	CALL SetComponent OF IBoundsComponent ON [TestSprite] WITH
		component AS [TestSpriteBounds]		
	GO
				
	UNNAME "TestSpriteTranslation" GO
	UNNAME "TestSpriteScale" GO
	UNNAME "TestSpriteAnimation" GO

	MAKE ClickComponent NAMED "TestSpriteClick" WITH
		bounds AS [TestSpriteBounds]
	GO

	SET Value ON [TestSpriteClick] AS 5
	GO

	SET Value ON [TestSpriteClick] AS
		READ OtherValue ON [TestSpriteClick] GO
	GO